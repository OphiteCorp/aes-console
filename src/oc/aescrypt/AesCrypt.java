package oc.aescrypt;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.*;

/**
 * Encrypt/Decrypt dat do AES-256 GCM.
 *
 * @author mimic
 */
public final class AesCrypt {

    public static boolean debug = false;

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    private static final String ALGORITHM = "AES";
    private static final String CIPHER_INSTANCE = "AES/GCM/NoPadding";
    private static final int AES_KEY_SIZE = 256;
    private static final int GCM_IV_LENGTH = 12;
    private static final int GCM_TAG_LENGTH = 16;

    private SecretKey secretKey;
    private byte[] iv;
    private boolean encryptWithIv;
    private boolean searchIvInEncrypt;
    private boolean insertIvToEncrypt;
    private boolean shuffleContent;

    /**
     * Nastaví tajný klíč. Pokud se nenastaví, tak bude vygenerován náhodný.
     *
     * @param secretKey Tajný klíč.
     */
    public void setSecretKey(byte[] secretKey) {
        if (secretKey != null) {
            try {
                this.secretKey = new SecretKeySpec(secretKey, ALGORITHM);
            } catch (Exception e) {
                if (debug) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Nastaví tajný klíč, který je v Base64. Pokud se nenastaví, tak bude vygenerován náhodný.
     *
     * @param secretKey Tajný klíč.
     */
    public void setSecretKey(String secretKey) {
        try {
            if (secretKey != null) {
                int delimIndex = secretKey.indexOf(":");
                if (delimIndex != -1) {
                    String type = secretKey.substring(0, delimIndex);
                    if ("pwd".equalsIgnoreCase(type)) {
                        byte[] passwordHash = toSha256v3(secretKey.substring(delimIndex + 1));
                        secretKey = toBase64(passwordHash);
                    } else {
                        secretKey = secretKey.substring(delimIndex + 1);
                    }
                }
            }
            byte[] secretKeyBytes = fromBase64(secretKey);
            setSecretKey(secretKeyBytes);

        } catch (Exception e) {
            if (debug) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Nastaví IV. Pokud se nenastaví, tak bude použit nulový byte.
     *
     * @param iv Musí mít délku {@link AesCrypt#GCM_IV_LENGTH}.
     */
    public void setIv(byte[] iv) {
        if (iv != null && iv.length == GCM_IV_LENGTH) {
            this.iv = iv;
            encryptWithIv = true;
        }
    }

    /**
     * Nastaví IV, které je v Base64. Pokud se nenastaví, tak bude použit nulový byte.
     *
     * @param ivInBase64 IV v Base64.
     */
    public void setIv(String ivInBase64) {
        try {
            byte[] iv = fromBase64(ivInBase64);
            setIv(iv);

        } catch (Exception e) {
            if (debug) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Povolí, aby byl při decryptu vyhledán a načten IV z encryptu.
     *
     * @param searchIvInEncrypt True, pokud se má pokusit vyhledat IV v encryptu.
     */
    public void setSearchIvInEncrypt(boolean searchIvInEncrypt) {
        this.searchIvInEncrypt = searchIvInEncrypt;
    }

    /**
     * Povolí vložení IV do encryptu.
     *
     * @param insertIvToEncrypt True, pokud se má vložit IV do encryptu.
     */
    public void setInsertIvToEncrypt(boolean insertIvToEncrypt) {
        this.insertIvToEncrypt = insertIvToEncrypt;
    }

    /**
     * Povolí náhodné zamíchání dat v encryptu.
     *
     * @param shuffleContent True, pokud se mají náhodně zamíchat data v encryptu pro těžší prolomení.
     */
    public void setShuffleContent(boolean shuffleContent) {
        this.shuffleContent = shuffleContent;
    }

    /**
     * Encrypt textu.
     *
     * @param text Vstupní text.
     * @return Výsledek.
     */
    public Result encrypt(String text) {
        if (text == null) {
            return null;
        }
        byte[] data = text.getBytes(StandardCharsets.UTF_8);
        return encrypt(data);
    }

    /**
     * Decrypt encryptovaného textu.
     *
     * @param inputData Vstupní encryptovaná data.
     * @return Výsledek.
     */
    public Result decrypt(String inputData) {
        if (inputData == null) {
            return null;
        }
        byte[] encrypted;
        int delimIndex = inputData.indexOf(":");
        if (delimIndex != -1) {
            String type = inputData.substring(0, delimIndex);
            if ("hex".equalsIgnoreCase(type)) {
                encrypted = fromHex(inputData.substring(delimIndex + 1));
            } else {
                encrypted = fromBase64(inputData.substring(delimIndex + 1));
            }
        } else {
            encrypted = fromBase64(inputData);
        }
        return decrypt(encrypted);
    }

    private Result encrypt(byte[] data) {
        return crypt(Cipher.ENCRYPT_MODE, secretKey, iv, data);
    }

    private Result decrypt(byte[] encrypted) {
        return crypt(Cipher.DECRYPT_MODE, secretKey, iv, encrypted);
    }

    private Result crypt(int cipherMode, SecretKey secret, byte[] iv, byte[] data) {
        if (data == null) {
            return null;
        }
        if (secret == null) {
            secret = generateSecret();

            if (secret == null) {
                return null;
            }
        }
        if (iv == null) {
            iv = createZeroIv();
        }
        try {
            boolean encryption = (cipherMode == Cipher.ENCRYPT_MODE);

            // v případě decryptu a vlastního IV, který je obsažen v encryptu
            if (!encryption && searchIvInEncrypt) {
                iv = Arrays.copyOfRange(data, data.length - GCM_IV_LENGTH, data.length);
                data = Arrays.copyOfRange(data, 0, data.length - GCM_IV_LENGTH);
            }
            Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
            SecretKeySpec spec = new SecretKeySpec(secret.getEncoded(), ALGORITHM);
            GCMParameterSpec gcmParam = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);

            cipher.init(cipherMode, spec, gcmParam);

            // akce před encryptací
            if (encryption) {
                if (shuffleContent) {
                    int seed = new Random().nextInt();
                    data = shuffleBySeed(data, seed);
                    data = insertSeedToData(data, seed);
                }
            }
            byte[] encrypted = cipher.doFinal(data);

            // akce po decrypt
            if (!encryption) {
                if (shuffleContent) {
                    int[] seedRef = new int[1];
                    encrypted = removeSeedFromData(encrypted, seedRef);
                    encrypted = unshuffleBySeed(encrypted, seedRef[0]);
                }
            } else {
                // v případě encryptu a vlastního IV
                if (encryptWithIv && insertIvToEncrypt) {
                    byte[] temp = Arrays.copyOf(encrypted, encrypted.length + iv.length);
                    System.arraycopy(iv, 0, temp, encrypted.length, iv.length);
                    encrypted = temp;
                }
            }
            return new Result(encrypted, secret.getEncoded(), iv, encryption);

        } catch (Exception e) {
            if (debug) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * Vygeneruje náhodný IV.
     *
     * @return Náhodný IV.
     */
    public static byte[] generateIv() {
        byte[] iv = new byte[GCM_IV_LENGTH];
        new SecureRandom().nextBytes(iv);
        return iv;
    }

    /**
     * Vytvoří nulový IV.
     *
     * @return Nulový IV.
     */
    public static byte[] createZeroIv() {
        byte[] iv = new byte[GCM_IV_LENGTH];
        Arrays.fill(iv, (byte) 0);
        return iv;
    }

    /**
     * Vygeneruje tajný klíč.
     *
     * @return Náhodný tajný klíč.
     */
    public static SecretKey generateSecret() {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM);
            keyGenerator.init(AES_KEY_SIZE);
            return keyGenerator.generateKey();

        } catch (Exception e) {
            if (debug) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * Převede data do Base64. Výstup je URL safe.
     *
     * @param data Vstupní data.
     * @return Data jako Base64.
     */
    public static String toBase64(byte[] data) {
        try {
            // je URL safe
            return (data != null) ? Base64.getUrlEncoder().encodeToString(data).split("=")[0] : null;
        } catch (Exception e) {
            if (debug) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * Převede Base64 zpět na původní data.
     *
     * @param base64 Data v Base64.
     * @return Původní data.
     */
    public static byte[] fromBase64(String base64) {
        try {
            return (base64 != null) ? Base64.getUrlDecoder().decode(base64) : null;
        } catch (Exception e) {
            if (debug) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * Převede vstupní text do SHA3-256.
     *
     * @param input Vstupní text.
     * @return Hash.
     */
    public static byte[] toSha256v3(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA3-256");
            return digest.digest(input.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            if (debug) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * Převede pole byte do HEX formátu.
     *
     * @param bytes Vstupní pole.
     * @return HEX formát.
     */
    public static String toHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];

        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Převede vstupní HEX do byte pole.
     *
     * @param hex Vstupní HEX.
     * @return Byte pole.
     */
    public static byte[] fromHex(String hex) {
        int length = hex.length();
        byte[] data = new byte[length / 2];

        for (int i = 0; i < length; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4) + Character.digit(hex.charAt(i + 1), 16));
        }
        return data;
    }

    private static byte[] insertSeedToData(byte[] data, int seed) {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(seed);
        byte[] seedBytes = buffer.array();
        byte[] temp = Arrays.copyOf(data, data.length + seedBytes.length);
        System.arraycopy(seedBytes, 0, temp, data.length, seedBytes.length);
        return temp;
    }

    private static byte[] removeSeedFromData(byte[] data, int[] seedRef) {
        byte[] timeData = Arrays.copyOfRange(data, data.length - Integer.BYTES, data.length);
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.put(timeData);
        buffer.flip();
        seedRef[0] = buffer.getInt();
        return Arrays.copyOfRange(data, 0, data.length - Integer.BYTES);
    }

    private static byte[] shuffleBySeed(byte[] data, int seed) {
        List<Byte> bytes = new ArrayList<>(data.length);

        for (byte b : data) {
            bytes.add(b);
        }
        Collections.shuffle(bytes, new Random(seed));

        for (int i = 0; i < data.length; i++) {
            data[i] = bytes.get(i);
        }
        return data;
    }

    private static byte[] unshuffleBySeed(byte[] data, int seed) {
        Random rand = new Random(seed);
        int[] seq = new int[data.length];

        for (int i = seq.length; i >= 1; i--) {
            seq[i - 1] = rand.nextInt(i);
        }
        for (int i = 0; i < seq.length; i++) {
            byte tmp = data[i];
            data[i] = data[seq[i]];
            data[seq[i]] = tmp;
        }
        return data;
    }

    /**
     * Výsledek pro encrypt/decrypt.
     */
    public static final class Result {

        public final byte[] data;
        public final byte[] secretKey;
        public final byte[] iv;

        private final boolean encrypted;

        private Result(byte[] data, byte[] secretKey, byte[] iv, boolean encrypted) {
            this.data = data;
            this.secretKey = secretKey;
            this.iv = iv;
            this.encrypted = encrypted;
        }

        /**
         * Vrátí data jako Base64. Většinou pro encrypt.
         *
         * @return Base64.
         */
        public String getDataAsBase64() {
            return (data != null) ? AesCrypt.toBase64(data) : "";
        }

        /**
         * Vrátí data jako text. Většinou pro decrypt.
         *
         * @return Text.
         */
        public String getDataAsString() {
            return (data != null) ? new String(data, StandardCharsets.UTF_8) : "";
        }

        /**
         * Vrátí data jako HEX formát. Většinou pro encrypt.
         *
         * @return HEX.
         */
        public String getDataAsHex() {
            return (data != null) ? AesCrypt.toHex(data) : "";
        }

        /**
         * Převede tajný klíč do Base64.
         *
         * @return Base64.
         */
        public String getSecretKeyAsBase64() {
            return (secretKey != null) ? AesCrypt.toBase64(secretKey) : "";
        }

        /**
         * Převede IV do Base64.
         *
         * @return Base64.
         */
        public String getIvAsBase64() {
            return (iv != null) ? AesCrypt.toBase64(iv) : "";
        }

        @Override
        public String toString() {
            return encrypted ? getDataAsBase64() : getDataAsString();
        }
    }
}
