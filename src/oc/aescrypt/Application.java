package oc.aescrypt;

import java.util.LinkedHashMap;
import java.util.Map;

public class Application {

    public static void main(String... args) {
        Map<String, String> params = readInputArgs(args);
        if (params == null) {
            return;
        }
        if (!params.containsKey("m")) {
            System.err.println("The '-m' parameter was not found");
            return;
        }
        if (!params.containsKey("d")) {
            System.err.println("The '-d' parameter was not found");
            return;
        }
        if (params.containsKey("debug")) {
            AesCrypt.debug = false;
        }
        String mode = params.get("m");
        String secretKey = params.get("k");
        String data = params.get("d");
        String outputType = params.get("o");

        AesCrypt crypt = new AesCrypt();
        crypt.setShuffleContent(true);
        crypt.setInsertIvToEncrypt(true);
        crypt.setSearchIvInEncrypt(true);

        AesCrypt.Result result;

        if ("encrypt".equalsIgnoreCase(mode)) {
            crypt.setSecretKey(secretKey);
            crypt.setIv(AesCrypt.generateIv());
            result = crypt.encrypt(data);

            if (result != null) {
                String encryptResult;

                if ("hex".equalsIgnoreCase(outputType)) {
                    encryptResult = result.getDataAsHex();
                } else {
                    encryptResult = result.getDataAsBase64();
                }
                if (secretKey == null) {
                    System.out.printf("%s|%s", result.getSecretKeyAsBase64(), encryptResult);
                } else {
                    System.out.print(encryptResult);
                }
            }
        } else if ("decrypt".equalsIgnoreCase(mode)) {
            crypt.setSecretKey(secretKey);
            result = crypt.decrypt(data);

            if (result != null) {
                System.out.print(result.getDataAsString());
            }
        } else {
            System.err.println("Invalid mode: [encrypt|decrypt]");
        }
    }

    private static Map<String, String> readInputArgs(String... args) {
        if (args.length == 0 || args.length % 2 != 0) {
            if (args.length == 0) {
                System.out.println("AES Encrypt/Decrypt Tool");
                System.out.println("Input:");
                System.out.println("-m = (required) modes [encrypt|decrypt]");
                System.out.println("-k = (optional) secret key (if not filled in, it will be generated); Lets you use the prefix pwd for your own password as pwd:password");
                System.out.println("-d = (required) data for encrypt/decrypt; You can use an encryption type prefix as hex:data");
                System.out.println("-o = (optional) encrypt type [hex]");
                System.out.println("Output:");
                System.out.println("- In case of encrypt - if the secret key is not filled in, it will be generated as the first output parameter (pipe separator \"|\")");
                System.out.println("- In all other cases, the result is always returned as a specific value. If the decrypt or something fails, null is returned.");
                System.out.println("Exanples:");
                System.out.println("app.jar -m encrypt -d \"message\"");
                System.out.println("app.jar -m encrypt -k \"pwd:my-password\" -d \"message\" -o hex");
                System.out.println("app.jar -m encrypt -k \"5765b9119834c0b14193f792ce86d0ff\" -d \"message\"");
                System.out.println("app.jar -m decrypt -k \"5765b9119834c0b14193f792ce86d0ff\" -d \"encrypted\"");
                System.out.println("app.jar -m decrypt -k \"pwd:my-password\" -d \"hex:encrypted\"");
            } else {
                System.err.println("Invalid number of parameters.");
            }
            return null;
        }
        Map<String, String> map = new LinkedHashMap<>();
        for (int i = 0; i < args.length; i += 2) {
            if (args[i].startsWith("-") && args[i + 1].length() > 0) {
                map.put(args[i].substring(1).toLowerCase(), args[i + 1]);
            }
        }
        return map;
    }
}
