# AES Console

A console application that contains the most secure and strong level of encryption.\
It uses **AES-256 GCM** with a random IV and password in **SHA3-256**.

## Requirements
- Only Java 13+

## Use

`java -jar aescrypt.jar -m encrypt -d "message"`\
Basic use of encrypt. In this case, a strong decrypt password will be generated. The output will be 2 strings separated by a pipe `|`, where the first is the hash for the decrypt and the second the encrypted text.

`java -jar aescrypt.jar -m decrypt -k "encrypt_hash" -d "encrypted_message"`\
Decrypt example for specified encrypt.

---

`java -jar aescrypt.jar -m encrypt -k "pwd:password" -d "message"`\
`java -jar aescrypt.jar -m decrypt -k "pwd:password" -d "encrypted_message"`\
Same example only with your own password.

`java -jar aescrypt.jar -m encrypt -k "pwd:password" -d "message" -o hex`\
The `-o hex` parameter converts the output encrypt to HEX format.

`java -jar aescrypt.jar -m decrypt -k "pwd:password" -d "hex:encrypted_message"`\
In this case, we also define the data type `-d` as `hex` for the decrypt.

---

`java -jar aescrypt.jar`\
If no parameter is specified, help is displayed.

## Real Examples

`java -jar aescrypt.jar -m encrypt -d "hello"`\
***Result:*** `rzzypMxcp_WSBibS1KZAvuOeYA4bsC1QepxxnrpCOtY|3Vho-Vo1JlM7WeaLCXKqsfF2y9_QPOV8IuxUwCS5VceVLBl7bA`

`java -jar aescrypt.jar -m decrypt -k "rzzypMxcp_WSBibS1KZAvuOeYA4bsC1QepxxnrpCOtY" -d "3Vho-Vo1JlM7WeaLCXKqsfF2y9_QPOV8IuxUwCS5VceVLBl7bA"`\
***Result:*** `hello`

---

`java -jar aescrypt.jar -m encrypt -d "hello" -o hex`\
***Result:*** `hl2swpVlBXMc7kJNnlfCy4X58PagjtIX20leO88yo84|456345E0336B516886D26ADEC766550C37FCADDB57990CEBB12DFFBDF4ECEF62B313235F01`

`java -jar aescrypt.jar -m decrypt -k "hl2swpVlBXMc7kJNnlfCy4X58PagjtIX20leO88yo84" -d "hex:456345E0336B516886D26ADEC766550C37FCADDB57990CEBB12DFFBDF4ECEF62B313235F01"`\
***Result:*** `hello`

---

`java -jar aescrypt.jar -m encrypt -k "pwd:123" -d "hello"`\
***Result:*** `M8IWGAXkhBR4Kj-0j127mHga70BC_9H1NGQpam04Nvva3AowPA`

`java -jar aescrypt.jar -m decrypt -k "pwd:123" -d "M8IWGAXkhBR4Kj-0j127mHga70BC_9H1NGQpam04Nvva3AowPA"`\
***Result:*** `hello`
